﻿using CicleSalesSystem.Domain.Entities.Entities.CicleSaleSystem;
using Dapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CicleSalesSystem.Infraestructure.Repositories.CicleSaleSystem
{
    public class ProductRepository : Domain.Interfaces.Repository.IProductRepository
    {
        private readonly Transversal.Common.Connection.IConnectionFactory _connectionFactory;
        public ProductRepository(Transversal.Common.Connection.IConnectionFactory connectionFactory)
                                 => _connectionFactory = connectionFactory;


        #region Async Methods
        public async Task<IEnumerable<Product>> GetAll()
        {
            var detener = "";
            using (var connection = _connectionFactory.GetConnection)
            {
                var sql = "select Id, Name, Description, UnitValue from products";
                //var products = await connection.QueryAsync<Domain.Entities.Entities.CicleSaleSystem.Product>
                //                                (sql, commandType: System.Data.CommandType.StoredProcedure);
                var products = await connection.QueryAsync<Domain.Entities.Entities.CicleSaleSystem.Product>
                                                (sql);
                return products;
            }

            //var products = new List<Domain.Entities.Entities.CicleSaleSystem.Product>()
            //{
            //    new Domain.Entities.Entities.CicleSaleSystem.Product(){ Id = System.Guid.NewGuid(), Name = "Pablin muchas gracias"},
            //    new Domain.Entities.Entities.CicleSaleSystem.Product(){ Id = System.Guid.NewGuid(), Name = "Usted es el mejor"},
            //    new Domain.Entities.Entities.CicleSaleSystem.Product(){ Id = System.Guid.NewGuid(), Name = "Lastima lo gay"}
            //};
            //return await Task.Run(() => products);
        } 
        #endregion
    }
}
