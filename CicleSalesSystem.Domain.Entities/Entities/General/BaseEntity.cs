﻿using System;

namespace CicleSalesSystem.Domain.Entities.Entities.General
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            this.CreatedAt = DateTime.Now;
        }
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
