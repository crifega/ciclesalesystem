﻿namespace CicleSalesSystem.Domain.Entities.Entities.CicleSaleSystem
{
    public class Product : General.BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitValue { get; set; }
    }
}
