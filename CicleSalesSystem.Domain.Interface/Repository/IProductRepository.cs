﻿namespace CicleSalesSystem.Domain.Interfaces.Repository
{
    public interface IProductRepository : General.IDomainRepositoryBase<Entities.Entities.CicleSaleSystem.Product>
    {

    }
}
