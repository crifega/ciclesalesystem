﻿namespace CicleSalesSystem.Domain.Interfaces.Domain
{
    #region Async Methods
    public interface IProductDomain : General.IDomainRepositoryBase<Entities.Entities.CicleSaleSystem.Product>
    {
    } 
    #endregion
}
