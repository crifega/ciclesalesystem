﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CicleSalesSystem.Domain.Interfaces.General
{
    public interface IDomainRepositoryBase<T> where T : Entities.Entities.General.BaseEntity
    {       
        #region Async Methods
        Task<IEnumerable<T>> GetAll(); 
        #endregion
    }
}
