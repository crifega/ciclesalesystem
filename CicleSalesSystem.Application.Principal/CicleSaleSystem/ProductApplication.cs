﻿using CicleSalesSystem.Application.DTO.CicleSaleSystem;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CicleSalesSystem.Application.Principal.CicleSaleSystem
{
    public class ProductApplication : Interfaces.CicleSaleSystem.IProductApplication
    {
        private readonly Domain.Interfaces.Domain.IProductDomain _productDomain;
        //private readonly AutoMapper.Mapper _mapper;
        public ProductApplication(Domain.Interfaces.Domain.IProductDomain productDomain)
        {
            _productDomain = productDomain;
           
        }

        public async Task<Transversal.Common.Response.ApiResponse<IEnumerable<ProductDTO>>> GetAll()
        {
            var response = new Transversal.Common.Response.ApiResponse<IEnumerable<ProductDTO>>();

            try
            {
                var productsDto = new List<ProductDTO>();
                
                var products = await _productDomain.GetAll();

                foreach (var item in products)
                {
                    var productDTO = new ProductDTO()
                    {
                        Id = item.Id,
                        Name = item.Name
                    };

                    productsDto.Add(productDTO);
                }


                response.Data = productsDto;
                response.Message = "OK";
                response.Result = true;
               
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
