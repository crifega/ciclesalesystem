﻿using CicleSalesSystem.Domain.Entities.Entities.CicleSaleSystem;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CicleSalesSystem.Domain.Core.CicleSaleSystem
{
    public class ProductDomain : Interfaces.Domain.IProductDomain
    {
        private readonly Interfaces.Repository.IProductRepository _productRepository;

        public ProductDomain(Interfaces.Repository.IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        #region Async Methods
        public async Task<IEnumerable<Product>> GetAll()
        {
            return await _productRepository.GetAll();
        } 
        #endregion
    }
}
