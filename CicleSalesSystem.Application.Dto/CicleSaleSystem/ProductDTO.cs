﻿using System;
namespace CicleSalesSystem.Application.DTO.CicleSaleSystem
{
    public class ProductDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitValue { get; set; }

    }
}
