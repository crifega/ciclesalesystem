﻿using AutoMapper;

namespace CicleSalesSystem.Transversal.Map
{
    public class MappingProfile : Profile
    {
        
        public MappingProfile()
        {
            base.CreateMap<Domain.Entities.Entities.CicleSaleSystem.Product, Application.DTO.CicleSaleSystem.ProductDTO>().ReverseMap();
        }
    }
}
