﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CicleSalesSystem.Application.Interfaces.General
{
    public interface IApplicationBase<T> where T : class
    {
        #region Async Methods
        Task<Transversal.Common.Response.ApiResponse<IEnumerable<T>>> GetAll();
        #endregion
    }
}
