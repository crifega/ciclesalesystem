﻿using CicleSalesSystem.Transversal.Common.Connection;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CicleSalesSystem.Infraestructure.Data.Connection
{
    public class ConnectionFactory : IConnectionFactory
    {
        public IDbConnection GetConnection
        {
            get
            {
                var sqlConnection = new SqlConnection();

                if (sqlConnection == null) return null;

                sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;    
                sqlConnection.Open();

                return sqlConnection;

            }
        }
    }
}
