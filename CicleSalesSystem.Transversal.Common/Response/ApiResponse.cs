﻿namespace CicleSalesSystem.Transversal.Common.Response
{
    public class ApiResponse<T>
    {
        public T Data { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
    }
}
