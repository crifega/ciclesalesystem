﻿using System.Data;

namespace CicleSalesSystem.Transversal.Common.Connection
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection { get; }
    }
}
