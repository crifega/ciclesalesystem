﻿using System.Web.Mvc;

namespace CicleSalesSystem.Services.WebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly Application.Interfaces.CicleSaleSystem.IProductApplication _productApplication;

      
        public HomeController(Application.Interfaces.CicleSaleSystem.IProductApplication productApplication)
        {
            _productApplication = productApplication;
        }
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            ViewBag.Title = "Home Page";
           var response = await _productApplication.GetAll();
            var resp = response.Data;
            ViewBag.response = await _productApplication.GetAll();

            return View(response.Data);
        }
    }
}
