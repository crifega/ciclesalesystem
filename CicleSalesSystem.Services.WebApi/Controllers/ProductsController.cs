﻿using System.Web.Http;

namespace CicleSalesSystem.Services.WebApi.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly Application.Interfaces.CicleSaleSystem.IProductApplication _productApplication;
        public ProductsController(Application.Interfaces.CicleSaleSystem.IProductApplication productApplication)
        {
            _productApplication = productApplication;
        }

        [HttpGet]
        public async System.Threading.Tasks.Task<IHttpActionResult> GetAllAsync()
        {
            var response = await _productApplication.GetAll();

            if (!response.Result) return BadRequest(response.Message);
            return Ok(response);
        }
    }
}
