﻿using Ninject;
using System;
using System.Collections.Generic;

namespace CicleSalesSystem.Services.WebApi.App_Start
{
    public class NinjectResolver : System.Web.Http.Dependencies.IDependencyResolver
    {
        private Ninject.IKernel kernel;

        public NinjectResolver() : this(new Ninject.StandardKernel())
        {

        }

        public NinjectResolver(Ninject.IKernel ninjectKernel, bool scope = false)
        {
            kernel = ninjectKernel;
            if (!scope)
            {
                AddBindings(kernel);
                AddRequestBindings(kernel);
            }
        }
        private void AddBindings(IKernel kernel)
        {
            kernel.Bind(typeof(Domain.Interfaces.Domain.IProductDomain)).To(typeof(Domain.Core.CicleSaleSystem.ProductDomain)).InSingletonScope();
            kernel.Bind(typeof(Domain.Interfaces.Repository.IProductRepository)).To(typeof(Infraestructure.Repositories.CicleSaleSystem.ProductRepository)).InSingletonScope();
            kernel.Bind(typeof(Transversal.Common.Connection.IConnectionFactory)).To(typeof(Infraestructure.Data.Connection.ConnectionFactory)).InSingletonScope();
        }

        private IKernel AddRequestBindings(IKernel kernel)
        {
            kernel.Bind<Application.Interfaces.CicleSaleSystem.IProductApplication>().To<Application.Principal.CicleSaleSystem.ProductApplication>();
            return kernel;
        }

        public System.Web.Http.Dependencies.IDependencyScope BeginScope()
        {
            return new NinjectResolver(AddRequestBindings(new Ninject.Extensions.ChildKernel.ChildKernel(kernel)), true);
        }

        public void Dispose()
        {
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
    }
}